<?php

namespace spec\App\Domain\User;

use App\Domain\User\UserId;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UserIdSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(UserId::class);
    }
}
