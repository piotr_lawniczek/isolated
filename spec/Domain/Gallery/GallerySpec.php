<?php

namespace spec\App\Domain\Gallery;

use App\Domain\Artwork\ArtworkId;
use App\Domain\Common\Exception\InvalidArgumentException;
use App\Domain\Artwork\Artwork;
use App\Domain\Gallery\Description;
use App\Domain\Gallery\Gallery;
use App\Domain\Gallery\GalleryId;
use App\Domain\Gallery\Name;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class GallerySpec extends ObjectBehavior
{
    const NAME = 'Art gallery';

    function let()
    {
        $this->beConstructedWith(new GalleryId, new Name(self::NAME));
    }

    function it_has_a_name()
    {
        $this->getName()->__toString()->shouldBe(self::NAME);
    }

    function it_has_no_description()
    {
        $this->getDescription()->shouldBe(null);
    }

    function it_sets_description(Description $description)
    {
        $this->setDescription($description);
        $this->getDescription()->shouldBe($description);
    }

    function it_adds_an_artwork_to_the_gallery(Artwork $artwork)
    {
        $artworkId = new ArtworkId;
        $artwork->getId()->willReturn($artworkId);
        $this->addArtwork($artwork);
        $this->containsArtwork($artwork)->shouldBe(true);
    }


    function it_checks_for_an_artwork_in_the_gallery(Artwork $artwork)
    {
        $artworkId = new ArtworkId;
        $artwork->getId()->willReturn($artworkId);
        $this->containsArtwork($artwork)->shouldBe(false);
        $this->addArtwork($artwork);
        $this->containsArtwork($artwork)->shouldBe(true);
    }

    function it_removes_an_artwork_from_the_gallery(Artwork $artwork)
    {
        $artworkId = new ArtworkId;
        $artwork->getId()->willReturn($artworkId);
        $this->addArtwork($artwork);
        $this->removeArtwork($artwork);
        $this->shouldBeEmpty();
    }

    function it_throws_exception_while_removing_not_contained_artwork(Artwork $artwork)
    {
        $this->shouldThrow(InvalidArgumentException::class)->during('removeArtwork', [$artwork]);
    }

    function it_checks_if_the_gallery_is_empty(Artwork $artwork)
    {
        $this->isEmpty()->shouldBe(true);
        $this->addArtwork($artwork);
        $this->shouldNotBeEmpty();
    }

    function it_empties_gallery(Artwork $artwork1, Artwork $artwork2)
    {
        $artwork1Id = new ArtworkId;
        $artwork2Id = new ArtworkId;
        $artwork1->getId()->willReturn($artwork1Id);
        $artwork2->getId()->willReturn($artwork2Id);
        $this->addArtwork($artwork1);
        $this->addArtwork($artwork2);
        $this->shouldNotBeEmpty();
        $this->empty();
        $this->shouldBeEmpty();
    }
}
