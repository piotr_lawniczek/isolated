<?php

namespace spec\App\Domain\Gallery;

use App\Domain\Common\ValueObject\AggregateId;
use App\Domain\Gallery\GalleryId;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class GalleryIdSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(AggregateId::class);
        $this->shouldHaveType(GalleryId::class);
    }
}
