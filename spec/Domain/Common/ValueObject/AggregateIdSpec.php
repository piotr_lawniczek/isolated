<?php

namespace spec\App\Domain\Common\ValueObject;

use App\Domain\Common\Exception\InvalidUUIDException;
use App\Domain\Common\ValueObject\AggregateId;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AggregateIdSpec extends ObjectBehavior
{
    function let()
    {
        $this->beAnInstanceOf(DummyConcreteAggregateId::class);
    }

    function it_throws_exception_after_construct_with_inproper_string()
    {
        $this
            ->shouldThrow(InvalidUUIDException::class)
            ->during('__construct', ['aaa']);
    }

    function it_can_be_constructed_with_proper_string()
    {
        $this
            ->shouldNotThrow(InvalidUUIDException::class)
            ->during('__construct', ['f6ebd2a8-1154-4f64-874b-b2d18e49c913']);
    }

    function it_can_be_constructed_with_no_arguments()
    {
        $this
            ->shouldNotThrow(InvalidUUIDException::class)
            ->during('__construct', []);
    }

    function it_can_be_casted_to_string()
    {
        $this->__toString()->shouldBeString();
    }

    function it_can_be_compared_against_other_aggregate_id()
    {
        $this->beConstructedWith('f6ebd2a8-1154-4f64-874b-b2d18e49c913');
        $this
            ->equals(new DummyConcreteAggregateId('f6ebd2a8-1154-4f64-874b-b2d18e49c913'))
            ->shouldReturn(true);
    }
}

class DummyConcreteAggregateId extends AggregateId
{

}
