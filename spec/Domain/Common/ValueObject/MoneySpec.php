<?php

namespace spec\App\Domain\Common\ValueObject;

use App\Domain\Common\ValueObject\Currency;
use App\Domain\Common\ValueObject\Money;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MoneySpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(10000, new Currency('EUR'));
    }

    function it_has_amount()
    {
        $this->getAmount()->shouldBe(10000);
    }

    function it_has_currency()
    {
        $this->getCurrency()->__toString()->shouldBe('EUR');
    }
}
