<?php

namespace spec\App\Domain\Common\ValueObject;

use App\Domain\Common\Exception\InvalidCurrencyCodeException;
use App\Domain\Common\ValueObject\Currency;
use PhpSpec\ObjectBehavior;

class CurrencySpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('EUR');
    }

    function it_throws_exception_when_wrong_code_given()
    {
        $this->shouldThrow(InvalidCurrencyCodeException::class)->during('__construct', ['QWER']);
    }

    function it_equals_same_currency()
    {
        $this->equals(new Currency('EUR'))->shouldBe(true);
    }

    function it_can_be_casted_to_string()
    {
        $this->__toString()->shouldBe('EUR');
    }
}
