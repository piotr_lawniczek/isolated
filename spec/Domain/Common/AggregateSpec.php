<?php

namespace spec\App\Domain\Common;

use App\Domain\Common\Aggregate;
use App\Domain\Common\ValueObject\AggregateId;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class AggregateSpec extends ObjectBehavior
{
    function let(AggregateId $aggregateId)
    {
        $this->beConstructedWith($aggregateId);
        $this->beAnInstanceOf(DummyConcreteAggregate::class);
    }
}

class DummyConcreteAggregate extends Aggregate
{

}
