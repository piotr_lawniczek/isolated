<?php

namespace spec\App\Domain\Grid;

use App\Domain\Artwork\Image;
use App\Domain\Grid\Height;
use App\Domain\Grid\Image as GridImage;
use App\Domain\Grid\Width;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ImageSpec extends ObjectBehavior
{
    function let(Image $image)
    {
        $this->beConstructedWith($image, new Width(1), new Height(1));
    }

    function it_extends_image()
    {
        $this->shouldHaveType(Image::class);
    }
}
