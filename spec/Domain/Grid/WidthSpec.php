<?php

namespace spec\App\Domain\Grid;

use App\Domain\Grid\Size;
use PhpSpec\ObjectBehavior;

class WidthSpec extends ObjectBehavior
{
    function it_is_size()
    {
        $this->shouldHaveType( Size::class);
    }
}
