<?php

namespace spec\App\Domain\Grid;

use App\Domain\Grid\Exception\NonPositiveSizeException;
use App\Domain\Grid\Size;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SizeSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(2);
    }

    function it_must_have_positive_value()
    {
        $this->shouldThrow(NonPositiveSizeException::class)->during('__construct', [-3]);
        $this->shouldThrow(NonPositiveSizeException::class)->during('__construct', [0]);
    }

    function it_equals_other_size()
    {
        $this->equals(new Size(2))->shouldBe(true);
    }

    function it_is_greater_than_other_size()
    {
        $this->isGreaterThan(new Size(1))->shouldBe(true);
    }

    function it_is_less_than_other_size()
    {
        $this->isLessThan(new Size(5))->shouldBe(true);
    }
}
