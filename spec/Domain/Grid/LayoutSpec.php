<?php

namespace spec\App\Domain\Grid;

use App\Domain\Artwork\Image;
use App\Domain\Gallery\Gallery;
use App\Domain\Grid\Height;
use App\Domain\Grid\Image as GridImage;
use App\Domain\Grid\Layout;
use App\Domain\Grid\Width;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LayoutSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Layout::class);
    }

    function let(Gallery $gallery)
    {
        $this->beConstructedWith($gallery, new Width(4), new Height(1));
    }

    function it_adds_image(GridImage $image)
    {
        $this->addImage($image);
        $this->getImages()[0]->shouldBe($image);
    }

    function it_removes_image(GridImage $image)
    {
        $this->removeImage($image);
        $this->reorganize()->shouldBeCalled();
    }

    function it_checks_if_contains(Image $image)
    {
        $this->containsImage($image)->shouldBe(false);
    }
}
