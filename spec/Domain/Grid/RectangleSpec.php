<?php

namespace spec\App\Domain\Grid;

use App\Domain\Grid\Height;
use App\Domain\Grid\Rectangle;
use App\Domain\Grid\Width;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RectangleSpec extends ObjectBehavior
{
    function let(Width $width, Height $height)
    {
        $this->beConstructedWith($width, $height);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Rectangle::class);
    }
}
