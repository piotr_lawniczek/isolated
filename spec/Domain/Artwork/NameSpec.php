<?php

namespace spec\App\Domain\Artwork;

use App\Domain\Common\Exception\InvalidArgumentException;
use App\Domain\Common\Artwork\Name;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class NameSpec extends ObjectBehavior
{
    const TEXT = 'Artwork name';

    function let()
    {
        $this->beConstructedWith(self::TEXT);
    }

    function it_has_text()
    {
        $this->getText()->shouldBe(self::TEXT);
    }

    function it_throws_exception_while_constructed_with_empty_string()
    {
        $this->shouldThrow(InvalidArgumentException::class)->during('__construct', ['']);
    }

    function it_can_be_casted_to_string()
    {
        $this->__toString()->shouldBeString();
    }
}
