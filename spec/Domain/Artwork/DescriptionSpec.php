<?php

namespace spec\App\Domain\Artwork;

use App\Domain\Common\Exception\InvalidArgumentException;
use App\Domain\Common\Artwork\Description;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class DescriptionSpec extends ObjectBehavior
{
    const TEXT = 'Artwork description';

    function let()
    {
        $this->beConstructedWith(self::TEXT);
    }

    function it_has_text()
    {
        $this->getText()->shouldBe(self::TEXT);
    }

    function it_throws_exception_while_constructed_with_empty_string()
    {
        $this->shouldThrow(InvalidArgumentException::class)->during('__construct', ['']);
    }

    function it_can_be_casted_to_string()
    {
        $this->__toString()->shouldBeString();
    }
}
