<?php

namespace spec\App\Domain\Artwork;

use App\Domain\Artwork\ArtworkId;
use App\Domain\Artwork\Image;
use PhpSpec\ObjectBehavior;
use App\Domain\Artwork\Name;
use App\Domain\Artwork\Description;

class ArtworkSpec extends ObjectBehavior
{
    const NAME = 'Art gallery';

    const DESCRIPTION = 'Art gallery description';

    function let()
    {
        $this->beConstructedWith(new ArtworkId(), new Name(self::NAME), new Description(self::DESCRIPTION));
    }

    function it_has_a_name()
    {
        $this->getName()->__toString()->shouldBe(self::NAME);
    }

    function it_has_a_description()
    {
        $this->getDescription()->__toString()->shouldBe(self::DESCRIPTION);
    }

    function it_adds_image(Image $image)
    {
        $this->addImage($image);
        $this->getImages()[0]->shouldBe($image);
    }

    function it_removes_image(Image $image)
    {
        $this->addImage($image);
        $this->removeImage($image);
        $this->getImages()->shouldBe([]);
    }
}
