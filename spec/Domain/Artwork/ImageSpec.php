<?php

namespace spec\App\Domain\Artwork;

use App\Domain\Artwork\Image;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ImageSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Image::class);
    }
}
