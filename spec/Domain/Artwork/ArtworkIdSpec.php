<?php

namespace spec\App\Domain\Artwork;

use App\Domain\Artwork\ArtworkId;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ArtworkIdSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(ArtworkId::class);
    }
}
