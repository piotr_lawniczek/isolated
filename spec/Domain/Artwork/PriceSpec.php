<?php

namespace spec\App\Domain\Artwork;

use App\Domain\Artwork\Price;
use App\Domain\Common\ValueObject\Money;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class PriceSpec extends ObjectBehavior
{
    function let(Money $money)
    {
        $this->beConstructedWith($money, true);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Price::class);
    }
}
