<?php

namespace App\Domain\Artwork;

use App\Domain\Common\Aggregate;
use App\Domain\Artwork\ArtworkId;
use App\Domain\Artwork\Name;
use App\Domain\Artwork\Description;

class Artwork extends Aggregate
{
    /**
     * @var Name
     */
    private $name;

    /**
     * @var Description
     */
    private $description;

    /**
     * Artwork constructor.
     * @param ArtworkId $id
     * @param Name $name
     * @param Description $description
     */
    public function __construct(ArtworkId $id, Name $name, Description $description)
    {
        parent::__construct($id);
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @param Name $name
     * @return Artwork
     */
    public function setName(Name $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Description
     */
    public function getDescription(): Description
    {
        return $this->description;
    }

    /**
     * @param Description $description
     * @return Artwork
     */
    public function setDescription(Description $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function addImage($argument1)
    {
        // TODO: write logic here
    }

    public function getImages()
    {
        // TODO: write logic here
    }

    public function removeImage($argument1)
    {
        // TODO: write logic here
    }
}
