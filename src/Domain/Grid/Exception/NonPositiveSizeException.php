<?php

namespace App\Domain\Grid\Exception;

use App\Domain\Common\Exception\InvalidArgumentException;

class NonPositiveSizeException extends InvalidArgumentException
{

}
