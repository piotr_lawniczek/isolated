<?php

namespace App\Domain\Common\ValueObject;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Money
 * @ORM\Embeddable()
 */
class Money
{
    // TODO: Implement static? currency calculator for exchange() method
    //      However it may be not the best idea to keep this logic in here

    /**
     * @var array
     */
    public const AVAILABLE_CURRENCIES = [
        'USD',
        'EUR'
    ];

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amount;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $currency;

    /**
     * Money constructor.
     * @param int $amount
     * @param string $currency
     * @throws \InvalidArgumentException
     */
    public function __construct(int $amount, string $currency)
    {
        $this->assertProperCurrency($currency);
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @param int $amount
     * @return Money
     * @throws \InvalidArgumentException
     */
    public static function USD(int $amount)
    {
        return new static($amount, 'USD');
    }
    /**
     * @param int $amount
     * @return Money
     * @throws \InvalidArgumentException
     */
    public static function EUR(int $amount)
    {
        return new static($amount, 'EUR');
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return bool
     */
    public function isNegative(): bool
    {
        return $this->amount < 0;
    }

    /**
     * @return bool
     */
    public function isPositive(): bool
    {
        return $this->amount > 0;
    }

    /**
     * @param Money $money
     * @return bool
     */
    public function isSameCurrency(Money $money): bool
    {
        return $this->currency === $money->getCurrency();
    }

    /**
     * @param Money $money
     */
    private function assertSameCurrency(Money $money): void
    {
        if (!$this->isSameCurrency($money)) {
            throw new \InvalidArgumentException('Money must have same currency.');
        }
    }

    /**
     * @param string $currency
     */
    private function assertProperCurrency(string $currency): void
    {
        if (!in_array($currency, self::AVAILABLE_CURRENCIES)) {
            throw new \InvalidArgumentException('Invalid currency');
        }
    }

    /**
     * @param Money $money
     * @return Money
     */
    public function add(Money $money): self
    {
        if ($this->isSameCurrency($money)) {
            return new self($this->amount + $money->getAmount(), $this->currency);
        }
        return new self($this->amount + $money->exchange($this->currency)->getAmount(), $this->currency);
    }

    /**
     * @param Money $money
     * @return Money
     */
    public function subtract(Money $money): self
    {
        if ($this->isSameCurrency($money)) {
            return new self($this->amount - $money->getAmount(), $this->currency);
        }
        return new self($this->amount - $money->exchange($this->currency)->getAmount(), $this->currency);
    }

    /**
     * @param Money $money
     * @return bool
     */
    public function equals(Money $money): bool
    {
        if ($this->isSameCurrency($money)) {
            return $this->amount === $money->getAmount();
        }
        return $this->amount === $money->exchange($this->currency)->getAmount();
    }

    /**
     * @param Money $money
     * @return bool
     */
    public function isLessThan(Money $money): bool
    {
        if ($this->isSameCurrency($money)) {
            return $this->amount < $money->getAmount();
        }
        return $this->amount < $money->exchange($this->currency)->getAmount();
    }

    /**
     * @param Money $money
     * @return bool
     */
    public function isGreaterThan(Money $money): bool
    {
        if ($this->isSameCurrency($money)) {
            return $this->amount > $money->getAmount();
        }
        return $this->amount > $money->exchange($this->currency)->getAmount();
    }

    /**
     * @param string $currency
     * @return Money
     */
    public function exchange(string $currency): self
    {
        $this->assertProperCurrency($currency);
        return new self($this->getAmount(), $currency);
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return number_format($this->amount / 100, 2) . ' ' . $this->currency;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
