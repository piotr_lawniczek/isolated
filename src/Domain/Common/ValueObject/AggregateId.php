<?php

namespace App\Domain\Common\ValueObject;

use App\Domain\Common\Exception\InvalidUUIDException;
use Ramsey\Uuid\Uuid;

abstract class AggregateId
{
    /**
     * @var string
     */
    private $uuid;

    /**
     * AggregateId constructor.
     * @param null|string $id
     * @throws InvalidUUIDException
     */
    public function __construct(?string $id = null)
    {
        try {
            $this->uuid = Uuid::fromString($id ?: (string)Uuid::uuid4())->toString();
        } catch (\InvalidArgumentException $e) {
            throw new InvalidUUIDException('Cannot create UUID from given string');
        }
    }

    /**
     * @param AggregateId $aggregateId
     * @return bool
     */
    public function equals(AggregateId $aggregateId): bool
    {
        return $this->uuid === $aggregateId->toString();
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
