<?php

namespace App\Domain\Common;

use App\Domain\Common\ValueObject\AggregateId;

/**
 * Class Aggregate
 * @package App\Domain\Common
 */
abstract class Aggregate
{
    /**
     * @var AggregateId
     */
    protected $id;

    /**
     * Aggregate constructor.
     * @param AggregateId $id
     */
    public function __construct(AggregateId $id)
    {
        $this->id = $id;
    }

    /**
     * @return AggregateId
     */
    public function getId(): AggregateId
    {
        return $this->id;
    }

    /**
     * @param Aggregate $aggregate
     * @return bool
     */
    public function equals(Aggregate $aggregate): bool
    {
        return $this->id->equals($aggregate->getId());
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getId();
    }
}
