<?php

namespace App\Domain\Gallery;

use App\Domain\Common\Exception\InvalidArgumentException;

/**
 * Class Name
 */
class Name
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var integer
     */
    private const MIN_LENGTH = 2;

    /**
     * Name constructor.
     * @param string $text
     * @throws InvalidArgumentException
     */
    public function __construct(string $text)
    {
        if (\strlen($text) < self::MIN_LENGTH) {
            throw new InvalidArgumentException('Name must has at least ' . self::MIN_LENGTH . ' letters');
        }
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getText();
    }
}
