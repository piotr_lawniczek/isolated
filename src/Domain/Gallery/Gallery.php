<?php

namespace App\Domain\Gallery;

use App\Domain\Artwork\Artwork;
use App\Domain\Common\Aggregate;
use App\Domain\Common\Exception\InvalidArgumentException;

/**
 * Class Gallery
 * @package App\Domain\Gallery
 */
class Gallery extends Aggregate
{
    /**
     * @var Name
     */
    private $name;

    /**
     * @var Description|null
     */
    private $description;

    /**
     * @var array
     */
    private $artworks = [];

    /**
     * Gallery constructor.
     * @param GalleryId $id
     * @param Name $name
     */
    public function __construct(GalleryId $id, Name $name)
    {
        parent::__construct($id);
        $this->name = $name;
    }

    /**
     * @param Name $name
     * @return Gallery
     */
    public function setName(Name $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    /**
     * @param Description|null $description
     * @return Gallery
     */
    public function setDescription(Description $description = null): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Description|null
     */
    public function getDescription(): ?Description
    {
        return $this->description;
    }

    /**
     * @param Artwork $artwork
     * @return Gallery
     * @throws InvalidArgumentException
     */
    public function addArtwork(Artwork $artwork): self
    {
        if ($this->containsArtwork($artwork)) {
            throw new InvalidArgumentException('Artwork is already in the gallery');
        }
        $this->artworks[] = $artwork;
        return $this;
    }

    /**
     * @param Artwork $artwork
     * @return bool
     */
    public function containsArtwork(Artwork $artwork): bool
    {
        foreach ($this->artworks as $contained) {
            if ($contained->getId()->equals($artwork->getId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return count($this->artworks) === 0;
    }

    /**
     * @return Gallery
     */
    public function empty(): self
    {
        array_map([$this, 'removeArtwork'], $this->artworks);
        return $this;
    }

    /**
     * @param Artwork $artwork
     * @return Gallery
     * @throws InvalidArgumentException
     */
    public function removeArtwork(Artwork $artwork): self
    {
        if (!$this->containsArtwork($artwork)) {
            throw new InvalidArgumentException('Artwork is not in the gallery');
        }
        $this->artworks = array_filter($this->artworks, function (Artwork $existing) use ($artwork) {
            return !$existing->getId()->equals($artwork->getId());
        });
        return $this;
    }
}
