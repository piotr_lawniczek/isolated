<?php

namespace App\Domain\User;

/**
 * Interface UserFactoryInterface
 */
interface UserFactoryInterface
{
    /**
     * @return User
     */
    public function register(): User;
}
